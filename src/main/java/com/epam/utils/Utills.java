package com.epam.utils;

import com.epam.bigtask.Word;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Utills {

  public static Map<String, Integer> sortingMap(Map<String, Integer> sentenceWordMap) {
    Map<String, Integer> sorted = sentenceWordMap.entrySet().stream()
            .sorted(Map.Entry.comparingByValue())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a1, a2) -> a1, LinkedHashMap::new));
    return sorted;
  }

  public static void printMap(Map<String, Integer> sentenceWordMap, String word, Word wordObject) {
    sentenceWordMap.put(word, Math.toIntExact(wordObject.getCountListWords()));
  }
}
