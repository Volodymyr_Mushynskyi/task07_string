package com.epam.manager;

import com.epam.bigtask.Letter;
import com.epam.bigtask.Sentences;
import com.epam.bigtask.Text;
import com.epam.bigtask.Word;
import com.epam.localization.menu.Printable;
import com.epam.localization.menu.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class Manager {

  private Text text;
  private Sentences sentences;
  private Word word;
  private Letter letter;
  private Map<String, String> subMenu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);
  private String string = null;

  private Locale locale;
  private ResourceBundle bundle;

  public Manager() {
    text = new Text();
    sentences = new Sentences();
    letter = new Letter();
    try {
      string = text.readTextFromFile();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    sentences.addSentence(string);

    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("Menu_sub", locale);
    createSubMenu();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::printSentencesList);
    methodsMenu.put("2", this::printMaxCountSentences);
    methodsMenu.put("3", this::printSentencesByNumberWords);
    methodsMenu.put("4", this::findUniqueWord);
    methodsMenu.put("5", this::findQuestionSentence);
    methodsMenu.put("6", this::printWordsAlphabeticOrder);
    methodsMenu.put("7", this::sortingByFirstConsonantLetter);
    methodsMenu.put("8", this::sortingByEnterLetter);
    methodsMenu.put("9", this::sortingWordsByNumberInSentence);
    methodsMenu.put("uk", this::ukrainianMenu);
  }

  private void createSubMenu() {
    subMenu = new LinkedHashMap<>();
    subMenu.put("1", bundle.getString("1"));
    subMenu.put("2", bundle.getString("2"));
    subMenu.put("3", bundle.getString("3"));
    subMenu.put("4", bundle.getString("4"));
    subMenu.put("5", bundle.getString("5"));
    subMenu.put("6", bundle.getString("6"));
    subMenu.put("7", bundle.getString("7"));
    subMenu.put("8", bundle.getString("8"));
    subMenu.put("9", bundle.getString("9"));
    subMenu.put("Q", bundle.getString("Q"));
  }

  private void ukrainianMenu() throws IOException, URISyntaxException {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("Menu_sub", locale);
    createSubMenu();
    showSubMenu(locale);
  }

  private void printMenuAction() {
    logger.info("--------------MENU-----------\n");
    for (String str : subMenu.values()) {
      logger.info(str);
    }
  }

  public void showSubMenu(Locale locale) throws IOException, URISyntaxException {
    String keyMenu;
    if (methodsMenu.containsKey(locale.getCountry())) {
      methodsMenu.get(locale.getCountry()).print();
    }
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      if (methodsMenu.containsKey(keyMenu)) {
        methodsMenu.get(keyMenu).print();
      }
    } while (!keyMenu.equalsIgnoreCase("Q"));
  }

  private void findQuestionSentence() {
    sentences.findQuestionSentence();
  }

  private void findUniqueWord() {
    sentences.findUniqueWordInFirstSentence();
  }

  private void printSentencesByNumberWords() {
    sentences.printSentenceByNumberWords();
  }

  private void printMaxCountSentences() {
    System.out.println(sentences.findMaxCountSentences());
  }

  private void printSentencesList() {
    sentences.printSentencesList();
  }

  private void printWordsAlphabeticOrder() {
    word.printWordsInAlphabeticOrder(string);
  }

  private void sortingByFirstConsonantLetter() {
    word.sortingByConsonantLetters(string);
  }

  private void sortingByEnterLetter() {
    letter.addLetters(string);
  }

  private void sortingWordsByNumberInSentence() {
    sentences.countWordsInSentence();
  }
}
