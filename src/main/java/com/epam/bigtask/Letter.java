package com.epam.bigtask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class Letter {

  private Word wordObject;
  private Map<String, Integer> letterMap = new TreeMap<>();
  private static Scanner input = new Scanner(System.in);
  private static Logger logger = LogManager.getLogger(Letter.class);
  private Integer numberLetter;

  public Letter() {
    wordObject = new Word();
  }

  public void addLetters(String text) {
    System.out.print("Enter letter :");
    String letter = input.nextLine();
    List<String> words = wordObject.divideSentenceByWords(text);
    words.stream()
            .filter(a -> a.matches("(?<!\\S)\\p{Alpha}+(?!\\S)"))
            .distinct()
            .filter(a -> splitWordByLetter(a, letter))
            .forEach(System.out::println);
    System.out.println();
    Map<String, Integer> sorted = letterMap.entrySet().stream()
            .sorted(Map.Entry.comparingByValue())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a1, a2) -> a1, LinkedHashMap::new));
    logger.info("Sorted map ");
    sorted.entrySet().stream()
            .forEach(System.out::println);
  }

  public boolean splitWordByLetter(String string, String searching) {
    numberLetter = 0;
    String[] arr = string.split("");
    for (int i = 0; i < arr.length; i++) {
      if (arr[i].equals(searching)) {
        ++numberLetter;
      }
    }
    letterMap.put(string, numberLetter);
    return true;
  }
}
