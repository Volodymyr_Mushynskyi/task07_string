package com.epam.bigtask;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Word {

  private String tempLetter = "";
  private String whiteSpace = " ";
  private long countListWords;
  private List<String> words;
  private Map<String, String> consonantMap = new TreeMap<>();

  public void setCountListWords(long countListWords) {
    this.countListWords = countListWords;
  }

  public long getCountListWords() {
    return this.countListWords;
  }

  public List<String> divideSentenceByWords(String sentence) {
    return words = Arrays.asList(sentence.split("[\\s.,;]+"));
  }

  public Map<String, Integer> countFrequencyWords(List<String> words) {
    Map<String, Integer> frequencyList;
    return frequencyList = words.stream()
            .collect(groupingBy(Function.identity(), summingInt(a -> 1)));
  }

  public void printWordsInAlphabeticOrder(String string) {
    List<String> words = divideSentenceByWords(string);
    words.stream()
            .filter(a -> a.matches("(?<!\\S)\\p{Alpha}+(?!\\S)"))
            .distinct()
            .sorted()
            .filter(a -> comparePreviousWords(a))
            .map(a -> a + " ")
            .forEach(System.out::print);
    System.out.println();
  }

  private boolean comparePreviousWords(String words) {
    words = String.valueOf(words.charAt(0));
    if (!words.equals(tempLetter)) {
      whiteSpace += " ";
      System.out.print("\n" + whiteSpace);
      tempLetter = words;
      return true;
    }
    tempLetter = words;
    return true;
  }

  public void sortingByConsonantLetters(String string) {
    List<String> words = divideSentenceByWords(string);
    words.stream()
            .filter(a -> a.matches("(?<!\\S)\\p{Alpha}+(?!\\S)"))
            .distinct()
            .filter(a -> findFirstConsonantLetter(a))
            .forEach(System.out::println);
    System.out.println();
    Map<String, String> sorted = consonantMap.entrySet().stream()
            .sorted(Map.Entry.comparingByValue())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a1, a2) -> a1, LinkedHashMap::new));
    sorted.entrySet().stream()
            .forEach(System.out::println);
  }

  private boolean findFirstConsonantLetter(String word) {
    List<Character> characters = word.chars()
            .mapToObj(item -> (char) item)
            .collect(Collectors.toList());
    String temporary = String.valueOf(characters.stream()
            .filter(a -> a.toString().matches("[a-zA-Z&&[^aeiouAEIOU]]{1}"))
            .findFirst());
    System.out.println(temporary);
    consonantMap.put(word, temporary);
    return true;
  }

  public boolean countNumberWordInSentence(String a, String word) {
    countListWords += countWordInEverySentence(word, a);
    return true;
  }

  private Integer countWordInEverySentence(String word, String sentence) {
    List<String> words = divideSentenceByWords(sentence);
    long count = words.stream()
            .filter(a -> a.equals(word))
            .count();
    return Math.toIntExact(count);
  }
}
