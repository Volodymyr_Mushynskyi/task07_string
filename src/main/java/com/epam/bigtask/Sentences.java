package com.epam.bigtask;

import com.epam.impl.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.epam.utils.Utills.printMap;
import static com.epam.utils.Utills.sortingMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Sentences implements Sentence {

  private Word wordObject;
  private List<String> sentences = new ArrayList<>();
  private Integer numberSentences = 0;
  private static Scanner input = new Scanner(System.in);
  private static Logger logger = LogManager.getLogger(Sentences.class);
  private List<String> letters = new ArrayList<>();
  private Map<String, Integer> sentenceWordMap = new TreeMap<>();
  private List<String> searchingWords;

  public Sentences() {
    wordObject = new Word();
  }

  public void addSentence(String sentence) {
    String[] parts = sentence.split("(?<=[.!?:])\\s");

    for (int i = 0; i < parts.length; i++) {
      System.out.println(parts[i]);
      sentences.add(parts[i]);
    }
  }

  public int findMaxCountSentences() {
    for (String sentence : sentences) {
      countSentenceWithSameWords(wordObject.divideSentenceByWords(sentence));
    }
    return numberSentences;
  }

  private int addNumberSentences() {
    return numberSentences++;
  }

  private void countSentenceWithSameWords(List<String> words) {
    Optional<String> match = wordObject.countFrequencyWords(words).entrySet().stream()
            .filter(a -> a.getValue().equals(2))
            .map(Map.Entry::getKey)
            .findFirst();
    match.stream()
            .filter(a -> !a.isEmpty())
            .forEach(a -> addNumberSentences());
  }

  public void printSentenceByNumberWords() {
    countWordsInSentence(getSentences()).entrySet().stream()
            .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
            .forEach(System.out::println);
  }

  private Map<String, Integer> countWordsInSentence(List<String> sentences) {
    Map<String, Integer> frequencyList;
    return frequencyList = sentences.stream()
            .collect(groupingBy(Function.identity(), summingInt(a -> wordObject.divideSentenceByWords(a).size())));
  }

  public List<String> getSentences() {
    return sentences;
  }

  private String getFirstSentence() {
    return sentences.get(0);
  }

  private TreeSet<String> convertToTreeSet(List<String> value) {
    TreeSet<String> set = new TreeSet<>(value);
    return set;
  }

  public void findUniqueWordInFirstSentence() {
    String otherSentence = sentences.stream()
            .filter(a -> !a.equals(getFirstSentence()))
            .collect(Collectors.joining());
    for (String string : convertToTreeSet(wordObject.divideSentenceByWords(getFirstSentence()))) {
      if (convertToTreeSet(wordObject.divideSentenceByWords(otherSentence)).add(string)) {
        System.out.println(string);
      }
    }
  }

  public void findQuestionSentence() {
    logger.info("Write size of words what are you looking");
    int length = input.nextInt();
    String s = sentences.stream()
            .filter(a -> a.matches("([^.?!]*)\\?"))
            .reduce("", String::concat);
    List<String> words = wordObject.divideSentenceByWords(s);
    words.stream()
            .filter(a -> a.length() == length)
            .distinct()
            .forEach(System.out::println);
  }

  public void countWordsInSentence() {
    createListSearchingWords();
    for (String word : searchingWords) {
      sentences.stream()
              .filter(a -> wordObject.countNumberWordInSentence(a, word))
              .count();
      printMap(sentenceWordMap, word, wordObject);
      wordObject.setCountListWords(0);
    }
    sortingMap(sentenceWordMap).entrySet().stream()
            .forEach(System.out::println);
  }

  private void createListSearchingWords() {
    searchingWords = new ArrayList<>();
    searchingWords.add("Sir");
    searchingWords.add("country");
    searchingWords.add("in");
    searchingWords.add("Churchill");
  }

  public void printSentencesList() {
    if (sentences.isEmpty())
      logger.info("PRINT LIST");
    for (String sentence : sentences) {
      logger.info(sentence);
    }
  }

  @Override
  public String toString() {
    return "Sentences{" +
            "sentences=" + sentences +
            '}';
  }
}
