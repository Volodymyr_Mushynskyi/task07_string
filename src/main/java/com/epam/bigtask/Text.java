package com.epam.bigtask;

import com.epam.impl.Sentence;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Text {

  private String data;
  private Sentence sentence;

  public Text() {
    this.sentence = new Sentences();
  }

  public String readTextFromFile() throws URISyntaxException, IOException {
    Path path = Paths.get(getClass().getClassLoader()
            .getResource("text.txt").toURI());
    Stream<String> lines = Files.lines(path);
    data = lines.collect(Collectors.joining(""));
    lines.close();
    return data;
  }
}
