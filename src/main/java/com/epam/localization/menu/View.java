package com.epam.localization.menu;

import com.epam.manager.Manager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class View {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  private Locale locale;
  private ResourceBundle bundle;

  private void createMenu() {
    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("Q", bundle.getString("Q"));
  }

  public View() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("Menu", locale);
    createMenu();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::ukrainianMenu);
    methodsMenu.put("2", this::englishMenu);
    methodsMenu.put("3", this::japanMenu);
    methodsMenu.put("4", this::arabicMenu);
    methodsMenu.put("5", this::printTasks);
  }

  private void ukrainianMenu() throws IOException, URISyntaxException {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("Menu", locale);
    createMenu();
    showMenu();
  }

  private void englishMenu() throws IOException, URISyntaxException {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("Menu", locale);
    createMenu();
    showMenu();
  }

  private void japanMenu() throws IOException, URISyntaxException {
    locale = new Locale("ja");
    bundle = ResourceBundle.getBundle("Menu", locale);
    createMenu();
    showMenu();
  }

  private void arabicMenu() throws IOException, URISyntaxException {
    locale = new Locale("ar");
    bundle = ResourceBundle.getBundle("Menu", locale);
    createMenu();
    showMenu();
  }

  private void printTasks() {
    try {
      new Manager().showSubMenu(locale);
    } catch (IOException | URISyntaxException e) {
      logger.error(e);
    }
  }

  private void printMenuAction() {
    logger.info("--------------MENU-----------\n");
    for (String str : menu.values()) {
      logger.info(str);
    }
  }

  public void showMenu() throws IOException, URISyntaxException {
    String keyMenu;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      if (methodsMenu.containsKey(keyMenu)) {
        methodsMenu.get(keyMenu).print();
      }
    } while (!keyMenu.equalsIgnoreCase("Q"));
  }
}
