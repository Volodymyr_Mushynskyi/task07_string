package com.epam.localization.menu;

import java.io.IOException;
import java.net.URISyntaxException;

@FunctionalInterface
public interface Printable {
  void print() throws IOException, URISyntaxException;
}
