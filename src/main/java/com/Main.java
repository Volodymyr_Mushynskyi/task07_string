package com;

import com.epam.localization.menu.View;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
  public static void main(String[] args) {
    try {
      new View().showMenu();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }
}
